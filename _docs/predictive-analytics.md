---
title: Predictive Analytics
permalink: /docs/predictive-analytics/
---

### Predictive Data Analytics Project Lifecycle: [CRISP-DM](https://en.wikipedia.org/wiki/Cross-industry_standard_process_for_data_mining)
```
1. Business Understanding
2. Data Understanding
3. Data Preparation
4. Modeling
5. Evaluation
6. Deployment
```

### 2. Data Understanding
#### Data Exproration
Data Quality Report: characteristics of each feature

For continuous features: count, %missing, cardinality(distinct value counts), min, Q1, mean, median, Q3, max, std

For categorical features: count, %missing, cardinality, mode, mode frequency, %mode, 2nd Mode, 2nd Mode Freq., 2nd %Mode

SPLOM(Scatter plot matrix)

covariance, correlation (normalized)

### 3. Data Preparation
ABT(Analytics Base Table)

Handling Time
  - Observation period: the descriptive features are calculated over this period
  - Outcome period: the target feature is calculated over this period

Observation period and outcome period can be defined by a fixed date or relative to an event

Next-best-offer model: Model to determine the least expensive incentive to retain potential churned users

Normalization: typically [0, 1] or [-1, 1]
Standardize: how far from mean. Unit is std

Binning
  - equal-width
  - equal-frequency

Sampling
  - Stratified sampling: divide into groups. equal % of sample from each group
  - Random sampling


### 4. Modeling

#### Information-based Learning: Decision trees

[Entropy](https://en.wikipedia.org/wiki/Entropy_(information_theory))
  - what: average rate at which information is produced by a stochastic source of data
  - how to measure impurity
    - entropy: the information entropy in bits is the base-2 logarithm (binary logarithm) of the number of possible outcomes N (log<sub>2</sub>N)
    - information gain ratio
    - Gini index

[Information gain](https://en.wikipedia.org/wiki/Information_gain_in_decision_trees)
  - what: the reduction in the entropy
  - how: [Kullback–Leibler divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence)

Inducing algorithm
  - ID3: build the tree based on information gain
  - C4.5
  - J48: Open source implementation of C4.5
  - CART(Classification And Regression Tree): uses the Gini index. can handle continuous target features

Advantage
  - Interpretable
  - can be used for categorical and continuous descriptive features
  - robust to noise if pruning is used

Drawbacks
  - with continuous feature, it tends to become quite large. not easy to interpret. error-based models may be more appropriate
  - with a large number of descriptive features and small number of instances in training dataset, overfitting becomes very likely. probability-based models do a better job with high-dimensional data
  - Not suitable for modeling concepts that change over time. similarity-based models performs better


Other concepts
  - Tree Pruning
  - Ensembles
    - Boosting
    - Bagging(Bootstrap aggregating)

#### Similarity-based Learning: kNN

`Key concepts: feature space, measures of similarity`

[Feature space](https://en.wikipedia.org/wiki/Feature_(machine_learning)#Extensions): one dimension for every descriptive feature. common to have thousands of descriptive features.

Criteria for a similarity metric
  - non-negativity
  - identity
  - symmetry
  - triangular inequality

  **if it does not satisfy all four of above criteria, we refer to measures of similarity of this type as indexes**

Ways to measure the similarity
  - Metric
    - Euclidean distance
    - Manhattan distance
    - Minkowski distance
    - Mahalanobis distance: for continuous descriptive features
  - Indexes
    - for Binary descriptive features: Russel-Rao, Sokal-Michener
    - for continuous descriptive features: Cosine similarity

How to choose right metric or indexes
  - appropriate for the properties of the dataset: binary/non-binary, sparse, covariant, ...
  - test

Data normalization
  - distance computations are sensitive to the value ranges of the features

How to set k?

[Feature selection](https://en.wikipedia.org/wiki/Feature_selection)
- greedy local search problem

Advantage
  - easy to interpret
  - relatively robust to noise
  - can be used for categorical and continuous descriptive features
  - lazy learner. robust to [concept drift](https://en.wikipedia.org/wiki/Concept_drift) than eager learning algorithms
Drawbacks
  - sensitive to curse of dimensionality
  - slower
  - may not be able to achieve the same levels of accuracy

Other concepts
  - Voronoi tessellation: a way of decomposing a space into regions
  - decision boundary: boundary between regions of the feature space in which different target levels
  - k-d tree
  - hyperplanes
  - hypersphere
  - weighted k nearest neighbor
  
#### Probability-based Learning: Naive Bayes model
`Key concepts: Bayes' Theorem`
  - There are two ways to reason with probabilities forward and inverse. Forward probability reasons from causes to effects: if we know that a particular causal event has happened, then we increase the probability associated with the known effects that it causes. Inverse probability reasons from effects to causes: if we know that a particular event has occurred, then we can increase the probability that one or more of the events that could cause the observed event have also happened.

Bayes' Theorem
  - The probability that an event has happened given a set of evidence for it is equal to the probability of the evidence being caused by the event multiplied by the probability of the event itself

Probability function
  - For categorical features: Probability mass functions
  - For continuous features: Probability density functions

#### Error-based Learning: Multivariable linear regression with gradient descent

Error function
  - sum of squared errors

Gradient Descent
Learning rates
Initial weights
SVM

Advantage
  - can be used for categorical and continuous descriptive features

#### Evaluation: misclassification rate

Designing Evaluation Experiments
  - confusion matrix
  - hold-out sampling
  - k-fold cross validation
  - leave-one-out cross validation
  - bootstrapping
  - out-of-time sampling

Performance Measures: Categorical Targets
  - Confusion matrix-based performance measures
  - Precision, Recall and F<sub>1</sub> measure
  - Average class accuracy
  - Measuring profit and loss

Performance Measures: Prediction Scores
  - ROC(Receiver Operating Characteristic) curves
  - K-S(Kolmogorov-Smirnov) Statistic
  - Measuring Gain and Lift

Performance Measures: Multinomial Targets

Performance Measures: Continuous Targets
  - sum of squared errors
  - Mean squared error(MSE)
  - Root mean squared error(RMSE)
  - Mean absolute error(MAE)
  - Domain Independent Measures of Error

Evaluating Models after Deployment
  - Monitoring change in Performance measures
  - Monitoring model output distribution changes: stability index
  - Monitoring descriptive feature distributin changes
  - Comparative Experiments using a control group

Popular measures for certain Industries
  - In financial credit scoring, Gini coefficient is almost always used to evaluate model performance

For categorical: average class accuracy based on a harmonic mean

For continuous: R<sup>2</sup> coefficient

